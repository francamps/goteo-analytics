<?php
    
    $table = 'project';

    // Local testing
    $mysqli = new mysqli("localhost", "root", "root", "goteo");

    // When in franc.ly
    //$mysqli = new mysqli('campsfebrercom.ipagemysql.com', 'francamps', 'goteando', 'goteo'); 

    $result = $mysqli->query('SELECT id FROM '.$table. ' WHERE published NOT LIKE "%NULL%" AND published > "2010-01-01"');

    $data = array();
    $html = "<select id='project_selector' name='project_selector'>";
    while ($row = $result->fetch_assoc()){
        $data[] = $row;
        $html .= "<option value='" . $row['id'] . "'>" . $row['id'];
    }
    $html .= '</select>';

    echo $html;

?>
