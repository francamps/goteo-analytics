<?php
    
    $table = 'project';
    $table2 = 'invest';
    $table3 = 'cost';
    $id = $_GET['id'];

    // Local testing
    $mysqli = new mysqli("localhost", "root", "root", "goteo");

    // When in franc.ly
    //$mysqli = new mysqli('campsfebrercom.ipagemysql.com', 'francamps', 'goteando', 'goteo'); 

    // List invests for the project -> Equivalent to getList for the model
    $result = $mysqli->query('SELECT * FROM '.$table. ' p JOIN ' .$table2. ' i ON i.project = p.id WHERE p.id = "'.$id.'"');
    $invests = array();
    while ($row = $result->fetch_assoc()){
        $invests[] = $row;
    }

    // Calculate publishing, success and passed dates -> Could be getMedium, although that's TMI?
    $result = $mysqli->query('SELECT published, closed, success, passed FROM '.$table. ' WHERE id = "'.$id.'"');
    $dates = array();
    while ($row = $result->fetch_assoc()){
        $dates = $row;
    }

    // Calculate minimum and optimum
    $result = $mysqli->query('SELECT sum(amount) as amount, c.required FROM '.$table3. ' c WHERE c.project = "'.$id.'" group by c.required');
    while ($row = $result->fetch_assoc()){
        if ($row['required'] == 1){
            $minimum = $row['amount'];
        } else {
            $optimum = $row['amount'];
        }
    }

    $data = array('invests' => $invests, 
                    'dates' => $dates,
                    'minimum' => $minimum,
                    'optimum' => $optimum);

    echo json_encode($data);

?>
